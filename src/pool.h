/*
 * pool.h
 */

#ifndef GUARD_POOL_H
#define GUARD_POOL_H

struct impl;

struct pool {
	struct impl *pimpl;
	int (*insert)(struct pool *ops, int value);
	int (*delete)(struct pool *ops, int value);
	int (*update)(struct pool *ops, int old_value, int new_value);
	int (*search)(struct pool *ops, int value);
	int (*dtor)(struct pool *ops);

	int (*simple_insert)(struct pool *ops, int value);
	int (*dump)(const struct pool *ops);
};

/*
 * リスト構造のプールのコンストラクタ
 */
struct pool *pool_list_ctor(void);

/*
 * ツリー構造のプールのコンストラクタ
 */
struct pool *pool_tree_ctor(void);

#endif // !GUARD_POOL_H
