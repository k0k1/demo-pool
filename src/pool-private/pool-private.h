/*
 * pool-private.h
 */

#ifndef GUARD_POOL_PRIVATE_H
#define GUARD_POOL_PRIVATE_H

#include "pool.h"
#include "list-private.h"
#include "tree-private.h"

union pool_impl {
	struct list list;
	struct tree tree;
};

struct impl {
	union pool_impl pool;
};

#endif // !GUARD_POOL_PRIVATE_H
