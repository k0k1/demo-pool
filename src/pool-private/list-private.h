/*
 * list-private.h
 */

#ifndef GUARD_LIST_PRIVATE_H
#define GUARD_LIST_PRIVATE_H

#include "sys/queue.h"

struct list_node {
	int value;
	TAILQ_ENTRY(list_node) entry;
};

TAILQ_HEAD(list, list_node);

#endif // !GUARD_LIST_PRIVATE_H
