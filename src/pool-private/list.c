/*
 * list.c
 */

#include <stdio.h>
#include <stdlib.h>

#include "pool.h"
#include "pool-private.h"
#include "list-private.h"

#ifndef TAILQ_FOREACH_SAFE
# define TAILQ_FOREACH_SAFE(var, head, field, tvar)	\
	for ((var) = TAILQ_FIRST(head);			\
	     (var) != NULL &&				\
	     ((tvar) = TAILQ_NEXT(var, field), 1);	\
	     (var) = (tvar))
#endif

static int
search(struct pool *ops, int value)
{
	struct list *list = &ops->pimpl->pool.list;
	struct list_node *node, *tnode;

	TAILQ_FOREACH_SAFE(node, list, entry, tnode) {
		if (node->value == value)
			return value;
	}
	// not found
	return -1;
}

static inline int __attribute__((always_inline))
insert__(struct pool *ops, int value)
{
	struct list *list = &ops->pimpl->pool.list;
	struct list_node *node = calloc(1, sizeof *node);

	node->value = value;
	TAILQ_INSERT_TAIL(list, node, entry);

	return 0;
}

static int
simple_insert(struct pool *ops, int value)
{
	return insert__(ops, value);
}

static int
insert(struct pool *ops, int value)
{
	if ((value < 0) || (search(ops, value) != -1)) {
		// already inserted
		return -1;
	}

	return insert__(ops, value);
}

static int
delete(struct pool *ops, int value)
{
	struct list *list = &ops->pimpl->pool.list;
	struct list_node *node, *tnode;

	TAILQ_FOREACH_SAFE(node, list, entry, tnode) {
		if (node->value == value) {
			TAILQ_REMOVE(list, node, entry);
			free(node);
			return 0;
		}
	}
	// not found
	return -1;
}

static int
update(struct pool *ops, int old_value, int new_value)
{
	if (search(ops, old_value) == -1) {
		// old_value is not found
		return -1;
	}

	if (search(ops, new_value) != -1) {
		// new_value alredy exists
		return -1;
	}

	delete(ops, old_value);
	insert(ops, new_value);

	return 0;
}

static int
dump(const struct pool *ops)
{
	struct list *list = &ops->pimpl->pool.list;
	struct list_node *node, *tnode;

	TAILQ_FOREACH_SAFE(node, list, entry, tnode) {
		printf(" %d", node->value);
	}
	puts("");
	return 0;
}

static int
dtor(struct pool *ops)
{
	struct list *list = &ops->pimpl->pool.list;
	struct list_node *node, *tnode;
	TAILQ_FOREACH_SAFE(node, list, entry, tnode) {
		TAILQ_REMOVE(list, node, entry);
		free(node);
	}

	struct impl *pimpl = ops->pimpl;
	free(pimpl);
	free(ops);

	return 0;
}

struct pool *
pool_list_ctor(void)
{
	struct impl *pimpl = calloc(1, sizeof *pimpl);
	TAILQ_INIT(&pimpl->pool.list);

	struct pool *ops = calloc(1, sizeof *ops);
	*ops = (struct pool) {
		.pimpl = pimpl,
		.insert = insert,
		.delete = delete,
		.update = update,
		.search = search,
		.dump = dump,
		.simple_insert = simple_insert,
		.dtor = dtor,
	};

	return ops;
}
