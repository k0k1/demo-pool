/*
 * tree.c
 */

#include <stdio.h>
#include <stdlib.h>

#include "pool.h"
#include "pool-private.h"
#include "tree-private.h"

static int
compare(struct tree_node *l, struct tree_node *r)
{
	if (l->value < r->value)
		return -1;
	else if (l->value > r->value)
		return 1;
	return 0;
}

RB_GENERATE_STATIC(tree, tree_node, entry, compare)

static int
search(struct pool *ops, int value)
{
	struct tree *tree = &ops->pimpl->pool.tree;
	struct tree_node tmp = { .value = value };
	struct tree_node *node = RB_FIND(tree, tree, &tmp);

	if (node)
		return 0;

	// not found
	return -1;
}

static inline int __attribute__((always_inline))
insert__(struct pool *ops, int value)
{
	struct tree *tree = &ops->pimpl->pool.tree;
	struct tree_node *node = calloc(1, sizeof *node);
	node->value = value;

	RB_INSERT(tree, tree, node);
	return 0;
}

static int
simple_insert(struct pool *ops, int value)
{
	return insert__(ops, value);
}

static int
insert(struct pool *ops, int value)
{
	if ((value < 0) || (search(ops, value) != -1)) {
		// an ignore value or already inserted
		return -1;
	}

	return insert__(ops, value);

}

static int
delete(struct pool *ops, int value)
{
	struct tree *tree = &ops->pimpl->pool.tree;
	struct tree_node tmp = { .value = value };
	struct tree_node *node = RB_FIND(tree, tree, &tmp);

	if (node) {
		RB_REMOVE(tree, tree, node);
		free(node);
		return 0;
	}

	return -1;
}

static int
update(struct pool *ops, int old_value, int new_value)
{
	if (search(ops, old_value) == -1) {
		// old_value is not found
		return -1;
	}

	if (search(ops, new_value) != -1) {
		// new_value alredy exists
		return -1;
	}

	delete(ops, old_value);
	insert(ops, new_value);

	return 0;
}

static int
dump(const struct pool *ops)
{
	struct tree *tree = &ops->pimpl->pool.tree;
	struct tree_node *node;

	RB_FOREACH(node, tree, tree) {
		printf(" %d", node->value);
	}
	puts("");
	return 0;
}

static int
dtor(struct pool *ops)
{
	struct tree *tree = &ops->pimpl->pool.tree;
	struct tree_node *node, *tmp;

	RB_FOREACH_SAFE(node, tree, tree, tmp) {
		RB_REMOVE(tree, tree, node);
		free(node);
	}

	struct impl *pimpl = ops->pimpl;
	free(pimpl);
	free(ops);

	return 0;
}

struct pool *
pool_tree_ctor(void)
{
	struct impl *pimpl = calloc(1, sizeof *pimpl);

	struct pool *ops = calloc(1, sizeof *ops);
	*ops = (struct pool) {
		.pimpl = pimpl,
		.insert = insert,
		.delete = delete,
		.update = update,
		.search = search,
		.dump = dump,
		.simple_insert = simple_insert,
		.dtor = dtor,
	};

	return ops;
}
