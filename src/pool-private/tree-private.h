/*
 * tree-private.h
 */

#ifndef GUARD_TREE_PRIVATE_H
#define GUARD_TREE_PRIVATE_H

#include <sys/tree.h>

struct tree_node {
	int value;
	RB_ENTRY(tree_node) entry;
};

RB_HEAD(tree, tree_node);

#endif // !GUARD_TREE_PRIVATE_H
