/*
 * main.c
 * [目的]
 * データ構造を選択し、選択したデータ構造が提供する方法でデータを挿入する。
 * データ検索での最悪値(＝見つけられなかったとき)をサイクルで表示する。
 */

// 表示とか乱数とかのためにインクルード
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <time.h>
#include <limits.h>

// CPUアフィニティやサイクル数取得のためにインクルード
#include <pthread.h>
#include <unistd.h>
#include <sys/user.h>
#include <sched.h>
#include <x86intrin.h>

// 調査対象のデータ構造(プール)と、その操作のためにインクルード
#include "pool.h"

typedef struct pool *(*pool_ctor_t)(void);

struct test_config {
	pool_ctor_t ctor;
	bool use_simple_insert;
	int nb_data;
	int loop;
};

// __rdtscをサポートしない環境向け
#if 0
unsigned long long
__attribute__((weak))
__rdtsc(void)
{
	union {
		unsigned long long tsc_64;
		struct {
			unsigned lo_32;
			unsigned hi_32;
		};
	} tsc;

	asm volatile("rdtsc" :
		     "=a" (tsc.lo_32),
		     "=d" (tsc.hi_32));
	return tsc.tsc_64;
}
#endif

/*
 * コマンドライン引数から、プールのコンストラクタを選択する。
 */
static pool_ctor_t
parse_commands(struct test_config *conf, int argc, char *argv[])
{
	// default configuration
	pool_ctor_t ctor = pool_tree_ctor;
	int nb_data = 1024, loop = 1;
	bool use_si = false;

	const struct option longopts[] = {
		{"list", no_argument, NULL, 'L'},
		{"tree", no_argument, NULL, 'T'},

		{"nbdata", required_argument, NULL, 'n'},
		{"loop", required_argument, NULL, 'l'},
		{"si", no_argument, NULL, 's'},
	};

	int ch;
	while ((ch = getopt_long_only(argc, argv, "LTn:l:s", longopts, NULL)) != -1) {
		switch (ch) {
		default:
			// FALLTHROUGH
		case 'T':
			ctor = pool_tree_ctor;
			break;
		case 'L':
			ctor = pool_list_ctor;
			break;

		case 'n':
			nb_data = atoi(optarg);
			break;
		case 'l':
			loop = atoi(optarg);
			break;
		case 's':
			use_si = true;
			break;
		}
	}

	conf->ctor = ctor;
	conf->nb_data = nb_data;
	conf->loop = loop;
	conf->use_simple_insert = use_si;

	return ctor;
}

/*
 * データ構造に挿入するデータ群を準備する
 */
static int *
prepare_test_data(const size_t len)
{
	int *test_data = calloc(len, sizeof(int));

	srand((unsigned int)clock());
	for (size_t i = 0; i < len; i++)
		test_data[i] = i;

	// シャッフルする
	for (size_t i = 0; i < len; i++) {
		size_t n = rand() % len;
		const int t = test_data[i];
		test_data[i] = test_data[n];
		test_data[n] = t;
	}

	puts("input are below...");
	for (size_t i = 0; i < len; i++)
		printf(" %d", test_data[i]);

	puts("\n-----");
	return test_data;
}

/*
 * どのコアでプロセスを実行するか指定する。
 */
static void
set_cpu_affinity(unsigned core_id)
{
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core_id, &cpuset);
	if (pthread_setaffinity_np(pthread_self(), sizeof cpuset, &cpuset)) {
		perror("pthread_setaffinity_np");
	}
}

/*
 *
 */
static inline int
measure_insert(struct pool *pool, const struct test_config * const conf, const int * const values)
{
	double measure_(struct pool *pool, const struct test_config * const conf, const int * const values) {
		typedef int (*insert_t)(struct pool *ops, int value);
		insert_t insert = conf->use_simple_insert ? pool->simple_insert : pool->insert;

		unsigned long long begin, end;

		begin = __rdtsc();
		for (int i = 0; i < conf->nb_data; i++)
			insert(pool, values[i]);
		end = __rdtsc();

		return ((double)(end - begin) / conf->nb_data);
	}

	printf("average cycles of data insertion is %lf\n", measure_(pool, conf, values));
	return 0;
}

/*
 * 検索回数の平均時間を算出する。
 */
static inline int
measure_hit_cycles(struct pool *pool, const int * const test_data, const int nb)
{
	double measure_(struct pool *pool, const int * const test_data, const int nb) {
		const int times = 2000;
		unsigned long long begin, end;

		begin = __rdtsc();
		for (int i = 0; i < times; i++)
			for (int n = 0; n < nb; n++)
				pool->search(pool, test_data[n]);

		end = __rdtsc();

		return ((double)(end - begin) / times / nb);
	}

	printf("average cycles of search is %lf\n", measure_(pool, test_data, nb));
	return 0;
}

/*
 * 最悪検索回数の平均時間を算出する。
 */
static inline int
measure_miss_cycles(struct pool *pool)
{
	double measure_(struct pool *pool) {
		const int times = 2000;
		unsigned long long begin, end;

		begin = __rdtsc();
		for (int i = 0; i < times; i++)
			// 見つけられない値(INT_MAX)で検索する
			pool->search(pool, INT_MAX);

		end = __rdtsc();

		return ((double)(end - begin) / times);
	}

	printf("average cycles of miss search is %lf\n", measure_(pool));
	return 0;
}

int
main(int argc, char *argv[])
{
	set_cpu_affinity(3);

	// コマンド引数を解析し、設定を決定する
	struct test_config config = {.nb_data = 0};
	pool_ctor_t ctor = parse_commands(&config, argc, argv);

	// プールに挿入するデータを作成し、表示する
	int *values = prepare_test_data(config.nb_data);

	// プールを作成する
	struct pool *pool = ctor();

	// プールにデータを挿入する
	measure_insert(pool, &config, values);

	// プールにあるデータを見る
	puts("\nresults are ...");
	if (pool->dump(pool) < 0) {
		fprintf(stderr, "error occured\n");
		return -1;
	}
	puts("-----");

	// 測定
	for (int i = 0; i < config.loop; i++) {
		measure_hit_cycles(pool, values, config.nb_data);
		measure_miss_cycles(pool);
	}

	// 後始末
	pool->dtor(pool);
	free(values);

	return 0;
}
