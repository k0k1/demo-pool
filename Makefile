PRJ_ROOT := $(PWD)
export PRJ_ROOT

.PHONY: all clean

all:
	$(MAKE) -C src

clean :
	$(MAKE) clean -C src
